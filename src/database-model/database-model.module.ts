import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../output/entities/user';



@Global()
@Module({
    imports: [TypeOrmModule.forRoot({
        type: "postgres",
        host: "127.0.0.1",
        port: 5432,
        username: "postgres",
        password: "postgres",
        database: "nestjs-userdb",
        schema: "public",
        synchronize: false,
        entities: [User]
      })],
      
})
export class DatabaseModelModule {}
