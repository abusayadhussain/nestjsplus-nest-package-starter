import { Entity } from "typeorm";
import { DatabaseModelModule } from "./database-model/database-model.module";
import { User } from "./output/entities/user";


export function getHello(): string {
  return 'Hello from the new package!';
}

export function databaseHandle(){
  return DatabaseModelModule;
}



export {User}
